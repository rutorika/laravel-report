<?php

namespace Rutorika\Report;

use Exception;
use Rutorika\Report\Models\Errors;

trait RutorikaReportExceptionTrait
{
    /**
     * Rutorika Report
     * Save exception to database
     * @param Exception $e
     * @return type
     */
    protected function rutorikaReportSaveError(Exception $e)
    {
        // Return if testing error or ulogged error

        if ($this->shouldntReport($e) || app()->runningUnitTests()) {
            return;
        }

        $model = new Errors();
        $model->fillEnv();
        $model->fillException($e);

        if (auth()->check()) {
            $model->fillUser(auth()->user());
        }

        if (!app()->runningInConsole()) {
            $model->fillRequest(request());
            $model->code = $this->isHttpException($e) ? $e->getStatusCode() : 500;
        }

        $model->save();
    }

//    /**
//     * Rutorika Report
//     * Save exception to database
//     * @param Exception $e
//     * @return type
//     */
//    protected function rutorikaReportSaveError(Exception $e)
//    {
//        try {
//
//            echo $b;
//
//            // Return if testing error or ulogged error
//
//            if ($this->shouldntReport($e) || app()->runningUnitTests()) {
//                return;
//            }
//
//            $model = new Errors();
//            $model->fillEnv();
//            $model->fillException($e);
//
//            if (auth()->check()) {
//                $model->fillUser(auth()->user());
//            }
//
//            if (!app()->runningInConsole()) {
//                $model->fillRequest(request());
//            }
//
//            $model->save();
//
//        } catch (Exception $e) {
//
//            $msg[] = "";
//            $msg[] = "";
//            $msg[] = "#### RUTORIKA REPORT ####";
//            $msg[] = "#### RUTORIKA REPORT " . $e->getMessage();
//            $msg[] = "#### RUTORIKA REPORT " . $e->getFile() . ' ' . $e->getLine();
//            $msg[] = "#### RUTORIKA REPORT ####";
//            $msg[] = "";
//
//            logger()->error(implode("\n", $msg));
//
//        }
//    }
}
