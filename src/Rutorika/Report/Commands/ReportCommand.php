<?php

namespace Rutorika\Report\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Rutorika\Report\Models\Errors;
use \Rutorika\Report\Drivers\Slack;

class ReportCommand extends Command
{
    protected $name = 'rutorika:report';

    protected $description = 'Send stored errors via slack';

    protected $lockfile = 'rutorika_report.lock';

    protected $batch = 100;

    // protected $signature = 'rutorika:exception-report';

    public function handle()
    {
        $this->line("");

        if (!$this->checkLockfile()) {
            return;
        }

        $total = 0;
        $slack = new Slack();

        while(true) {

            $errors = $this->getBatchErrors();

            if ($errors->count() == 0) {
                break;
            }

            foreach ($errors as $row) {
                if (true === $slack->send($row)) {
                    $row->is_slack_sended = true;
                    $row->save();
                }
            }

            $total += $errors->count();
        }

        if ($total == 0) {
            $this->result("No unsent reports");
            return;
        }

        $this->line(sprintf("%s reports sent", $total));
        $this->result('Complete');
    }

    protected function getBatchErrors()
    {
        return Errors::where('is_slack_sended', '0')
            ->orderBy('created_at', 'asc')
            ->limit($this->batch)
            ->get();
    }

    protected function checkLockfile()
    {
        $lockfile = storage_path($this->lockfile);

        if (file_exists($lockfile)) {
            $this->result("The lockfile {$lockfile} is found. Exit");
            return false;
        }

        file_put_contents($lockfile, time());

        register_shutdown_function(function() use($lockfile) {
            unlink($lockfile);
        });

        return true;
    }

    protected function result($message)
    {
        $this->line($message);
        $this->line("");
    }
}
