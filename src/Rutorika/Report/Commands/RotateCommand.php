<?php

namespace Rutorika\Report\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Rutorika\Report\Models\Errors;
use Carbon\Carbon;
use DB;

class RotateCommand extends Command
{
    protected $name = 'rutorika:report-rotate';

    protected $description = 'Delete old errors';

    protected $signature = 'rutorika:report-rotate {--L|maxlife=30 : Days}';

    public function handle()
    {
        $this->line("");

        $maxlife = $this->getMaxlife();

        $date = Carbon::today()->subDays($maxlife);
        $rows = DB::delete("DELETE FROM rt_errors WHERE is_slack_sended = '1' AND created_at < ?", [date($date)]);

        if ($rows == 0) {
            $message = 'There is nothing for rotate';
        } else {
            $message = $rows . ' rows deleted';
        }

        $this->line($message);
        $this->line("");
    }

    protected function getMaxlife()
    {
        $value = (int) $this->option('maxlife');

        if (!empty($value)) {
            return $value;
        }

        return 30;
    }
}
