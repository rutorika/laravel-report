<?php

namespace Rutorika\Report\Models;

/**
 * Errors
 *
 * @property bigint    $id
 * @property text      $env
 * @property boolean   $cli
 * @property integer   $user_id
 * @property integer   $code
 * @property text      $method
 * @property boolean   $ajax
 * @property text      $url
 * @property text      $params
 * @property text      $file
 * @property text      $line
 * @property text      $message
 * @property text      $trace
 * @property boolean   $is_slack_sended
 * @property boolean   $is_email_sended
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class Errors extends \Eloquent
{
    protected $table = 'rt_errors';

    protected $fillable = [
        'env',
        'cli',
        'user_id',
        'code',
        'method',
        'ajax',
        'url',
        'params',
        'file',
        'line',
        'message',
        'trace',
        'is_slack_sended',
        'is_email_sended',
    ];

    protected $guarded = ['id'];

//    public function user()
//    {
//        return $this->belongsTo(Users::class, 'user_id', 'id');
//    }

    /**
     *
     * @return $this
     */
    public function fillEnv()
    {
        if (app()->runningInConsole()) {
            $this->cli = true;
        } else {
            $this->cli = false;
        }

        $this->env = app()->environment();

        return $this;
    }

    /**
     *
     * @param \App\Models\Users $u
     * @return $this
     */
    public function fillUser($u)
    {
        $this->user_id = $u->id;

        return $this;
    }

    /**
     *
     * @param \Exception $e
     * @return $this
     */
    public function fillException($e)
    {
        $this->message = $e->getMessage();
        $this->file    = $e->getFile();
        $this->line    = $e->getLine();

        return $this;
    }

    /**
     *
     * @param \App\Models\Illuminate\Http\Request $r
     * @return $this
     */
    public function fillRequest($r)
    {
        $this->method = $r->method();
        $this->url    = $r->url();
        $this->ajax   = $r->ajax();

        $i = $r->input();
        if (!empty($i)) {
            $this->params = json_encode($i);
        }

        return $this;
    }
}
