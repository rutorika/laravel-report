<?php

namespace Rutorika\Report;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../../config.php' => config_path('rutorika/report.php'),
        ]);

        $this->loadMigrationsFrom(__DIR__ . '/../../database/migrations');

        if ($this->app->runningInConsole()) {
            $this->commands(
                Commands\ReportCommand::class,
                Commands\RotateCommand::class
            );
        }
    }

    public function provides()
    {
        return [];
    }
}