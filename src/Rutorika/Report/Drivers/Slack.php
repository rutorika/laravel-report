<?php

namespace Rutorika\Report\Drivers;

use Rutorika\Report\Models\Errors;
use RuntimeException;
use GuzzleHttp\Client as Guzzle;

class Slack
{
    protected $config;

    protected $guzzle;

    public function __construct()
    {
        $this->guzzle = new Guzzle();

        $config = config()->get('rutorika.report.slack');
        $this->setConfig($config);
    }

    public function setConfig(array $config)
    {
        $this->config = $config;

        return $this;
    }

    public function setGuzzle(Guzzle $guzzle)
    {
        $this->guzzle = $guzzle;

        return $this;
    }

    /**
     * Send message to slack
     * @param mixed $object
     * @param array $params
     * @param array $params[channel]
     * @param array $params[username]
     * @param array $params[icon_emoji]
     * @return boolean
     * @throws RuntimeException
     */
    public function send($object, $params = array())
    {
        $message = $this->getMessage($object, $params);

        $json = json_encode($message, JSON_UNESCAPED_UNICODE);

        if ($json === false) {
            throw new RuntimeException(sprintf('JSON encoding error %s: %s', json_last_error(), json_last_error_msg()));
        }

        $result = $this->guzzle->post($this->config['endpoint'], ['body' => $json]);

        return true;
    }

    /**
     * Build slack message
     * @param mixed $object
     * @param array $params
     * @param array $params[channel]
     * @param array $params[username]
     * @param array $params[icon_emoji]
     * @return text
     * @throws RuntimeException
     */
    protected function getMessage($object, $params)
    {
        if (is_scalar($object)) {
            $text = $object;
        } else if ($object instanceof Errors) {
            $text = $this->getErrorText($object);
            $attach = $this->getErrorAttach($object);
        } else {
            throw new RuntimeException('Incorrect object type');
        }

        if (empty($params['channel'])) {
            $params['channel'] = $this->config['channel'];
        }

        if (empty($params['username'])) {
            $params['username'] = $this->config['username'];
        }

        if (empty($params['icon_emoji'])) {
            $params['icon_emoji'] = $this->config['icon_emoji'];
        }

        $message = [
            'text'       => $text,
            'channel'    => $params['channel'],
            'username'   => $params['username'],
            'icon_emoji' => $params['icon_emoji'],
            'mrkdwn'     => true,
        ];

        if (!empty($attach)) {
            $message['attachments'] = [$attach];
        }

        return $message;
    }

    protected function getErrorText(Errors $e)
    {
        $code = $e->cli ? '' : $e->code . ' ';

        return '*Error: ' . $code . $e->message . '*';
    }

    protected function getErrorAttach(Errors $e)
    {
        $fields[] = [
            'title' => $e->file . ' ' . $e->line,
            'short' => false
        ];

        if ($e->cli) {

            $fields[] = [
                'value' => 'Cli mode',
                'short' => false
            ];

        } else {

            $fields[] = [
                'value' => 'Request: ' . $e->method . ' ' . $e->url,
                'short' => false
            ];

            if (!empty($e->params)) {
                $p = json_decode($e->params);
                if (!empty($p)) {
                    $a[] = 'Request params';
                    foreach ($p as $k => $v) {
                        $a[] = $k . ': ' . $v;
                    }
                    $fields[] = [
                        'value' => implode("\n", $a),
                        'short' => false
                    ];
                }
            }

            if (!empty($e->ajax)) {
                $fields[] = [
                    'value' => 'Ajax request',
                    'short' => false
                ];
            }
        }

        // if (!empty($e->user_id)) {
        //    $text = '*User:* ' . $e->user->fullname . ' ' . $e->user->role;
        // }

        $fields[] = [
            'value' => 'Env: ' . $e->env,
            'short' => false
        ];

        $fields[] = [
            'value' => $e->created_at->format('d/m/Y H:i:s'),
            'short' => false
        ];

        return [
            'color'    => "#F35A00",
            'fields'   => $fields
        ];
    }
}

