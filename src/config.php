<?php

return [

    'slack' => [

        'endpoint' => env('RUTORIKA_REPORT_SLACK_WEBHOOK_ENDPOINT', null),

        'channel' => env('RUTORIKA_REPORT_SLACK_CHANNEL', '#general'),

        'username' => env('RUTORIKA_REPORT_SLACK_USERNAME', 'Robot'),

        'icon_emoji' => env('RUTORIKA_REPORT_SLACK_ICON_EMOJI', ':beetle:'),

    ]

];
