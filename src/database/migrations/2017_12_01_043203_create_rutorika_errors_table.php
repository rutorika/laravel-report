<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRutorikaErrorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rt_errors', function(Blueprint $t) {

            $t->bigIncrements('id');
            $t->text('env')->nullable();
            $t->boolean('cli')->default(false);
            $t->integer('user_id')->nullable();
            $t->smallInteger('code')->nullable();
            $t->text('method')->nullable();
            $t->text('url')->nullable();
            $t->boolean('ajax')->default(false);
            $t->text('params')->nullable();
            $t->text('file')->nullable();
            $t->text('line')->nullable();
            $t->text('message')->nullable();
            $t->text('trace')->nullable();
            $t->boolean('is_slack_sended')->default(false);
            $t->boolean('is_email_sended')->default(false);
            $t->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $t->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));

            $t->index('created_at');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rt_errors');
    }
}
