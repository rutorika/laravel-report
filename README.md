
Пакет логирует исключения в базе данных (таблица **rt_errors**) и отправляет уведомления в Slack (в дальнейшем будут добавлены другие получатели).

Зависимости:

- **php >=7.0.0**
- **laravel >= 5.5**
- **guzzlehttp/guzzle >=6.3**


## Develop

```
"repositories": [
    {
        "type": "path",
        "url": "/srv/www/rutorika-report"
    }
]

composer require rutorika/laravel-report:dev-master --prefer-source
```

## Production

Добавить в **composer.json** репозиторий пакета:

```
"repositories": [
      {
        "type": "vcs",
        "url": "git@bitbucket.org:rutorika/laravel-report.git"
      }
]
```

Выполнить команды:

```
composer require rutorika/laravel-report:dev-master
php artisan vendor:publish --provider="Rutorika\Report\ServiceProvider"
php artisan config:clear
php artisan config:cache
php artisan migrate
```

В файл конфигурации **.env** добавить и настроить параметры:

- **RUTORIKA_REPORT_SLACK_WEBHOOK_ENDPOINT** - адрес хука Slack
- **RUTORIKA_REPORT_SLACK_CHANNEL** - канал Slack
- **RUTORIKA_REPORT_SLACK_USERNAME** - имя отправителя (по-умолчанию *Robot*)
- **RUTORIKA_REPORT_SLACK_ICON_EMOJI** - пикторграмма отправилеля (по-умолчанию *:beetle:*)


## Включение логирования Exceptions

Для включения логирования необходимо в **App\Exceptions\Handler** подключить трейт **RutorikaReportExceptionTrait**

```
<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Rutorika\Report\RutorikaReportExceptionTrait;

class Handler extends ExceptionHandler
{
    use RutorikaReportExceptionTrait;

    ...

    public function report(Exception $exception)
    {
        $this->rutorikaReportSaveError($exception);

        parent::report($exception);
    }

    ...
}
?>
```

## Отправка и ротация отчетов

Для отправки и ротации отчетов в crontab необходимо добавить вызовы команд:

- php artisan rutorika:report
- php artisan rutorika:report-rotate --maxlife=30




